Source: jsofa
Section: java
Priority: optional
Maintainer: Debian Astro Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper-compat (= 13),
               default-jdk,
               maven-debian-helper
Build-Depends-Indep: default-jdk-doc,
                     libmaven-antrun-plugin-java,
                     libwagon-java,
                     libwagon-ssh-java,
                     libmaven-javadoc-plugin-java
Standards-Version: 4.5.1
Homepage: https://javastro.github.io/jsofa/
Vcs-Git: https://salsa.debian.org/debian-astro-team/jsofa.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/jsofa
Rules-Requires-Root: no

Package: libjsofa-java
Multi-Arch: foreign
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: libjsofa-java-doc, ${maven:OptionalDepends}
Description: Pure Java translation of the IAU's C SOFA software library
 jsofa provides algorithms and software for use in astronomical
 computing, including routines for Astrometry, Calendars, Times,
 Coordinates etc.
 .
 This is a java translation of the official IAU SOFA library.

Package: libjsofa-java-doc
Multi-Arch: foreign
Section: doc
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Recommends: ${java:Recommends}
Suggests: libjsofa-java
Description: Pure Java translation of the IAU's C SOFA software library (docs)
 jsofa provides algorithms and software for use in astronomical
 computing, including routines for Astrometry, Calendars, Times,
 Coordinates etc. It is a java translation of the official IAU SOFA
 library.
 .
 This is the documentation of the jsofa library.
